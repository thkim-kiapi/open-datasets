# KIAPI Open Datasets
<img src="/uploads/ca2b1de377750f4d7f07e5d4949dad50/kiapi_ci_lr.png" width="20%" height="20%" align="right">

## Korea Intelligent Automotive Parts Promotion Institute (KIAPI)
<img src="/uploads/6a88b16e6f2332222606b8d4e78bc587/kiapi_location.png" width="45%" height="45%" align="left">

### Purpose of Establishment
KIAPI will support evaluation of parts of autonomous vehicles and ADAS for local auto parts suppliers with limited development and continuous growth due to changes in internal and external condition, and to become a necessary facility in the local auto parts industry.
Contributing to the strengthening and development of international advanced competitiveness of high premium auto parts industry and related industries through infrastructure construction and efficient management and operation to foster the rights of auto parts makers and to foster ITS-based automotive parts base valley.

### KIAPI Proving Ground
##### Location and Scale
201, Gukgasandanseo-ro, Guji-myeon, Dalseong-gun, Daegu, 43011, Republic of Korea.
Test track of 394,565m² and operation of related euipment.
<br/>
<br/>
<hr/>

## Datasets

### About
 We present a dataset that contains sensory information and annotations. We recorded several downtown and suburbs of Daegu, Republic of Korea. We annotate both static and dynamic scene elements and transfer this information into the image domain, resulting in annotations for both 3D point clouds and 2D images.
<img src="/uploads/a54f84433f55742f3f8302031c921827/kiapi_dataset_env.png" width="100%" height="100%" align="center">

### Sensors
<img src="/uploads/0068df88970d2e95354eca641b78c3f6/kiapi_av_ioniqev.png" width="75%" height="75%" align="right">

##### Lidar
- Manufacturer: Velodyne
- Model: VLP-32C
##### Front Camera
 - Manufacturer: IDS Imaging
 - Model: U3-3060Cp-C-HQ Rev.2
<br/>
 In addition, our system is equipped with an IMU/GPS (OxTS RT-3002) localization system.
 
### Examples
![kiapi_dataset_example](/uploads/bc0918beb556a1da16513cc29e605006/kiapi_dataset_example.png)

### Download Link
- [ ] Dataset #1 [Download](http://www.kiapi.or.kr)
- [ ] Dataset #2 [Download](http://gw.kiapi.or.kr)
